﻿using System;


namespace LunarDust
{
    /// <summary>
    /// Класс Soap содержит набор свойств, 
    /// отображающих информацию об экземпляре мыла.
    /// </summary>
    public class Soap
    {
     public Soap(int id, string name, string description, string description_full, byte[] image)
        {
            Id = id;
            Name = name;
            Description = description;
            Description_full = description_full;
            Image = Convert.ToBase64String(image);
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Description_full { get; set; }

        public string Image { get; set; }
    }
}