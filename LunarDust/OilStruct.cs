﻿

namespace LunarDust
{
    /// <summary>
    /// Структура Oil содержит набор свойств, 
    /// отображающих навзание и свойства масла.
    /// </summary>
    public struct Oil
    {
        public string Name { get; set; }
        public decimal Hardness { get; set; }
        public decimal CleansingQualities { get; set; }
        public decimal EmollientQualities { get; set; }
        public decimal FoamBubble { get; set; }
        public decimal FoamCreaminess { get; set; }
        public decimal IodineNumber { get; set; }
        public decimal Saponification { get; set; }
    }
}