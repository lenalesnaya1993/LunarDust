﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SoapCalculator.ascx.cs" Inherits="LunarDust.SoapCalculator" %>


<div id="calculator" class="calculator">
    <div class="getsmaller"><h2>Мыльный калькулятор</h2></div>
    <div class="space getsmaller"><p>..............</p></div>
    <ul>
        <li class="oilsWeight">
            <div class="percents recipe"><p>Компоненты</p></div>
            <ul>
                <li class="recipeinfo">
                    <div class="percents inerval"><span>Процент воды к маслу</span></div>
                    <div class="percents"><span>Процент свободных жиров</span></div>
                </li>
                <li class="recipeinfo recipelastelement">
                    <asp:DropdownList ID="DropdownWater" runat ="server" cssclass="buttons drop">
                         <asp:ListItem Value="25">25%</asp:ListItem>
                         <asp:ListItem Value="27">27%</asp:ListItem>
                         <asp:ListItem Value="30">30%</asp:ListItem>
                         <asp:ListItem Value="33">33%</asp:ListItem>
                         <asp:ListItem Value="35">35%</asp:ListItem>
                         <asp:ListItem Value="38">38%</asp:ListItem>
                    </asp:DropdownList>
                    <div class="transposition"></div>
                    <asp:DropdownList ID="DropdownOil" runat="server" cssclass="buttons drop">
                         <asp:ListItem Value="100">0%</asp:ListItem>
                         <asp:ListItem Value="99">1%</asp:ListItem>
                         <asp:ListItem Value="98">2%</asp:ListItem>
                         <asp:ListItem Value="97">3%</asp:ListItem>
                         <asp:ListItem Value="96">4%</asp:ListItem>
                         <asp:ListItem Value="95">5%</asp:ListItem>
                         <asp:ListItem Value="94">6%</asp:ListItem>
                         <asp:ListItem Value="93">7%</asp:ListItem>
                         <asp:ListItem Value="92">8%</asp:ListItem>
                         <asp:ListItem Value="91">9%</asp:ListItem>
                         <asp:ListItem Value="90">10%</asp:ListItem>
                         <asp:ListItem Value="89">11%</asp:ListItem>
                         <asp:ListItem Value="88">12%</asp:ListItem>
                         <asp:ListItem Value="87">13%</asp:ListItem>
                         <asp:ListItem Value="86">14%</asp:ListItem>
                         <asp:ListItem Value="85">15%</asp:ListItem>
                    </asp:DropdownList>
                </li>
            </ul>
            <div class="space"><p>..............</p></div>
            <div class="openoilsbutton"><a href="#oilslink" target="_self" title="Выбрать"><div>Выбрать масла</div></a></div>
            <div class="space"><p>..............</p></div>
            <ul>
                <li class="recipeinfo">
                    <div class="percents">
                        <asp:Label ID="oilLabel0" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilLabel1" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilLabel2" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilLabel3" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilLabel4" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilLabel5" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilLabel6" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilLabel7" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilLabel8" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilLabel9" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilLabel10" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilLabel11" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilLabel12" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilLabel13" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilLabel14" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilLabel15" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                    </div>
                </li>
                <li class="recipeinfo recipelastelement biggerli">
                    <div class="percents">
                        <asp:TextBox ID="oilTextBox0" runat="server" Visible="false" Width="40" MaxLength="5" CssClass="grammtextbox" CausesValidation="False"></asp:TextBox>
                        <asp:Label ID="g0" runat="server" Text="г." Visible="false"></asp:Label>
                        <asp:TextBox ID="oilTextBox1" runat="server" Visible="false" Width="40" MaxLength="5" CssClass="grammtextbox" ></asp:TextBox>
                        <asp:Label ID="g1" runat="server" Text="г." Visible="false"></asp:Label>
                        <asp:TextBox ID="oilTextBox2" runat="server" Visible="false" Width="40" MaxLength="5" CssClass="grammtextbox" ></asp:TextBox>
                        <asp:Label ID="g2" runat="server" Text="г." Visible="false"></asp:Label>
                        <asp:TextBox ID="oilTextBox3" runat="server" Visible="false" Width="40" MaxLength="5" CssClass="grammtextbox" ></asp:TextBox>
                        <asp:Label ID="g3" runat="server" Text="г." Visible="false"></asp:Label>
                        <asp:TextBox ID="oilTextBox4" runat="server" Visible="false" Width="40" MaxLength="5" CssClass="grammtextbox" ></asp:TextBox>
                        <asp:Label ID="g4" runat="server" Text="г." Visible="false"></asp:Label>
                        <asp:TextBox ID="oilTextBox5" runat="server" Visible="false" Width="40" MaxLength="5" CssClass="grammtextbox" ></asp:TextBox>
                        <asp:Label ID="g5" runat="server" Text="г." Visible="false"></asp:Label>
                        <asp:TextBox ID="oilTextBox6" runat="server" Visible="false" Width="40" MaxLength="5" CssClass="grammtextbox" ></asp:TextBox>
                        <asp:Label ID="g6" runat="server" Text="г." Visible="false"></asp:Label>
                        <asp:TextBox ID="oilTextBox7" runat="server" Visible="false" Width="40" MaxLength="5" CssClass="grammtextbox" ></asp:TextBox>
                        <asp:Label ID="g7" runat="server" Text="г." Visible="false"></asp:Label>
                        <asp:TextBox ID="oilTextBox8" runat="server" Visible="false" Width="40" MaxLength="5" CssClass="grammtextbox" ></asp:TextBox>
                        <asp:Label ID="g8" runat="server" Text="г." Visible="false"></asp:Label>
                        <asp:TextBox ID="oilTextBox9" runat="server" Visible="false" Width="40" MaxLength="5" CssClass="grammtextbox" ></asp:TextBox>
                        <asp:Label ID="g9" runat="server" Text="г." Visible="false"></asp:Label>
                        <asp:TextBox ID="oilTextBox10" runat="server" Visible="false" Width="40" MaxLength="5" CssClass="grammtextbox" ></asp:TextBox>
                        <asp:Label ID="g10" runat="server" Text="г." Visible="false"></asp:Label>
                        <asp:TextBox ID="oilTextBox11" runat="server" Visible="false" Width="40" MaxLength="5" CssClass="grammtextbox" ></asp:TextBox>
                        <asp:Label ID="g11" runat="server" Text="г." Visible="false"></asp:Label>
                        <asp:TextBox ID="oilTextBox12" runat="server" Visible="false" Width="40" MaxLength="5" CssClass="grammtextbox" ></asp:TextBox>
                        <asp:Label ID="g12" runat="server" Text="г." Visible="false"></asp:Label>
                        <asp:TextBox ID="oilTextBox13" runat="server" Visible="false" Width="40" MaxLength="5" CssClass="grammtextbox" ></asp:TextBox>
                        <asp:Label ID="g13" runat="server" Text="г." Visible="false"></asp:Label>
                        <asp:TextBox ID="oilTextBox14" runat="server" Visible="false" Width="40" MaxLength="5" CssClass="grammtextbox" ></asp:TextBox>
                        <asp:Label ID="g14" runat="server" Text="г." Visible="false"></asp:Label>
                        <asp:TextBox ID="oilTextBox15" runat="server" Visible="false" Width="40" MaxLength="5" CssClass="grammtextbox" ></asp:TextBox>
                        <asp:Label ID="g15" runat="server" Text="г." Visible="false"></asp:Label>
                    </div>
                </li>
            </ul>
            <asp:Button ID="CalcButton" cssclass="buttons" runat="server" Text="Рассчитать" OnClick="CalcButton_Click" Font-Italic="False" />
        </li>
        <li class="oilsWeight lastelement">
            <div class="percents recipe"><p>Рецепт</p></div>
            <ul>
                <li class="recipeinfo">
                    <div class="percents"><span>Процент воды к маслу  </span></div>
                    <div class="percents"><span>Свободных жиров  </span></div>
                    <div class="space"><p>..............</p></div>
                    <div class="percents"><span>Щёлочь NaOH  </span></div>
                    <div class="percents"><span>Вода  </span></div>
                    <div class="percents">
                        <asp:Label ID="oilSecondLabel0" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilSecondLabel1" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilSecondLabel2" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilSecondLabel3" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilSecondLabel4" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilSecondLabel5" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilSecondLabel6" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilSecondLabel7" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilSecondLabel8" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilSecondLabel9" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilSecondLabel10" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilSecondLabel11" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilSecondLabel12" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilSecondLabel13" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilSecondLabel14" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilSecondLabel15" runat="server" Text="" Visible="false" CssClass="transposition"></asp:Label>
                    </div>
                    <div class="space"><p>..............</p></div>
                    <div class="percents"><span>Итоговый вес мыла  </span></div>
                    <div class="space"><p>..............</p></div>
                </li>
                <li class="recipeinfo recipelastelement">
                    <div class="percents"><asp:Label ID="calc1" runat="server" Text="-"></asp:Label></div>
                    <div class="percents"><asp:Label ID="calc2" runat="server" Text="-"></asp:Label></div>
                    <div class="space"><p></p></div>
                    <div class="percents"><asp:Label ID="lyelabel" runat="server" Text="0 г."></asp:Label></div>
                    <div class="percents"><asp:Label ID="waterlabel" runat="server" Text="0 г."></asp:Label></div>
                    <div class="percents">
                        <asp:Label ID="oilWeight0" runat="server" Text="0 г." Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilWeight1" runat="server" Text="0 г." Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilWeight2" runat="server" Text="0 г." Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilWeight3" runat="server" Text="0 г." Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilWeight4" runat="server" Text="0 г." Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilWeight5" runat="server" Text="0 г." Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilWeight6" runat="server" Text="0 г." Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilWeight7" runat="server" Text="0 г." Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilWeight8" runat="server" Text="0 г." Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilWeight9" runat="server" Text="0 г." Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilWeight10" runat="server" Text="0 г." Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilWeight11" runat="server" Text="0 г." Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilWeight12" runat="server" Text="0 г." Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilWeight13" runat="server" Text="0 г." Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilWeight14" runat="server" Text="0 г." Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilWeight15" runat="server" Text="0 г." Visible="false" CssClass="transposition"></asp:Label>
                    </div>
                    <div class="space"><p></p></div>
                    <div class="percents"><asp:Label ID="weightlabel" runat="server" Text="0 г."></asp:Label></div>
                    <div class="space"><p></p></div>
                </li>
                <li class="recipeinfo recipelastelement">
                    <div class="percents"><asp:Label ID="waterpercentlabel" runat="server" Text="%"></asp:Label></div>
                    <div class="percents"><asp:Label ID="oilpercentlabel" runat="server" Text="%"></asp:Label></div>
                    <div class="space"><p></p></div>
                    <div class="percents"><asp:Label ID="calc3" runat="server" Text="-"></asp:Label></div>
                    <div class="percents"><asp:Label ID="calc4" runat="server" Text="-"></asp:Label></div>
                    <div class="percents">
                        <asp:Label ID="oilPercent0" runat="server" Text="%" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilPercent1" runat="server" Text="%" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilPercent2" runat="server" Text="%" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilPercent3" runat="server" Text="%" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilPercent4" runat="server" Text="%" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilPercent5" runat="server" Text="%" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilPercent6" runat="server" Text="%" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilPercent7" runat="server" Text="%" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilPercent8" runat="server" Text="%" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilPercent9" runat="server" Text="%" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilPercent10" runat="server" Text="%" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilPercent11" runat="server" Text="%" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilPercent12" runat="server" Text="%" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilPercent13" runat="server" Text="%" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilPercent14" runat="server" Text="%" Visible="false" CssClass="transposition"></asp:Label>
                        <asp:Label ID="oilPercent15" runat="server" Text="%" Visible="false" CssClass="transposition"></asp:Label>
                    </div>
                    <div class="space"><p></p></div>
                    <div class="percents"><asp:Label ID="calc5" runat="server" Text="-"></asp:Label></div>
                    <div class="space"><p></p></div>
                </li>
            </ul>
            <div class="percents recipe"><p>Сбалансированность рецепта</p></div>
            <ul>
                <li class="recipeinfo">
                    <div class="optimalword"><p></p></div>
                    <div class="percents"><span>Твёрдость  </span></div>
                    <div class="percents"><span>Очищающие качества  </span></div>
                    <div class="percents"><span>Смягчающие качества  </span></div>
                    <div class="percents"><span>Пузыристость пены  </span></div>
                    <div class="percents"><span>Кремовость пены  </span></div>
                    <div class="percents"><span>Йодное число  </span></div>
                </li>
                <li class="recipeinfo recipelastelement">
                    <div class="optimalword"><p></p></div>
                    <div class="percents"><asp:Label ID="hardnesslabel" runat="server" Text="0"></asp:Label></div>
                    <div class="percents"><asp:Label ID="cleansinglabel" runat="server" Text="0"></asp:Label></div>
                    <div class="percents"><asp:Label ID="emollientlabel" runat="server" Text="0"></asp:Label></div>
                    <div class="percents"><asp:Label ID="bubblelabel" runat="server" Text="0"></asp:Label></div>
                    <div class="percents"><asp:Label ID="creaminesslabel" runat="server" Text="0"></asp:Label></div>
                    <div class="percents"><asp:Label ID="iodlabel" runat="server" Text="0"></asp:Label></div>
                </li>
                <li class="recipeinfo recipelastelement optimal">
                    <div class="optimalword"><p>Оптимально</p></div>
                    <div class="percents"><asp:Label ID="calc6" runat="server" Text="35-45" ForeColor="#AAAAAA"></asp:Label></div>
                    <div class="percents"><asp:Label ID="calc7" runat="server" Text="15-20" ForeColor="#AAAAAA"></asp:Label></div>
                    <div class="percents"><asp:Label ID="calc8" runat="server" Text="50-70" ForeColor="#AAAAAA"></asp:Label></div>
                    <div class="percents"><asp:Label ID="calc9" runat="server" Text="15-30" ForeColor="#AAAAAA"></asp:Label></div>
                    <div class="percents"><asp:Label ID="calc10" runat="server" Text="15-35" ForeColor="#AAAAAA"></asp:Label></div>
                    <div class="percents"><asp:Label ID="calc11" runat="server" Text=" 0-60" ForeColor="#AAAAAA"></asp:Label></div>
                </li>
            </ul>
        </li>
    </ul>
    <div id="oilslink">
        <div class="oilswindow">
                <asp:CheckBoxList id="chooseOils" AutoPostBack="False" CellPadding="5" CellSpacing="5" RepeatColumns="1"
                RepeatDirection="Vertical" RepeatLayout="Flow" TextAlign="Right" runat="server">
                 <asp:ListItem>Абрикосовых косточек масло</asp:ListItem>
                 <asp:ListItem>Авокадо масло</asp:ListItem>
                 <asp:ListItem>Виноградных косточек масло</asp:ListItem>
                 <asp:ListItem>Жожоба масло</asp:ListItem>
                 <asp:ListItem>Зародышей пшеницы масло</asp:ListItem>
                 <asp:ListItem>Какао масло</asp:ListItem>
                 <asp:ListItem>Касторовое масло</asp:ListItem>
                 <asp:ListItem>Кокосовое масло</asp:ListItem>
                 <asp:ListItem>Макадамии масло</asp:ListItem>
                 <asp:ListItem>Миндаля сладкого масло</asp:ListItem>
                 <asp:ListItem>Оливковое масло</asp:ListItem>
                 <asp:ListItem>Пальмовое масло</asp:ListItem>
                 <asp:ListItem>Пальмоядровое масло</asp:ListItem>
                 <asp:ListItem>Персиковых косточек масло</asp:ListItem>
                 <asp:ListItem>Подсолнечное масло</asp:ListItem>
                 <asp:ListItem>Ши масло</asp:ListItem>
             </asp:CheckBoxList>
            <div class="button"><asp:Button id="saveOils" runat="server" Text="Закрыть" OnClick="SaveOilsButton_Click" /></div>
        </div>
     </div>
    </div>
