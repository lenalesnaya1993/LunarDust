﻿using System.Collections.Generic;


namespace LunarDust
{
    /// <summary>
    /// Класс OilsList служит для создания списка 
    /// используемых в калькуляторе масел.
    /// </summary>
    public static class OilsList
    {
        /// <summary>
        /// Метод ReturnListOfOilsMethod создаёт 
        /// список экземпляров структуры Oil,
        /// заполняет его данными о каждом масле 
        /// и возвращает список.
        /// </summary>
        /// <returns>Список масел.</returns>
        public static List<Oil> ReturnListOfOilsMethod()
        {
            List<Oil> oils = new List<Oil>();

            Oil apricot = new Oil
            {
                Name = "Масло абрикосовых косточек",
                Hardness = 6m,
                CleansingQualities = 0m,
                EmollientQualities = 93m,
                FoamBubble = 0m,
                FoamCreaminess = 6m,
                IodineNumber = 100m,
                Saponification = 0.1350m
            };
            oils.Add(apricot);
            Oil avocado = new Oil
            {
                Name = "Масло авокадо",
                Hardness = 18m,
                CleansingQualities = 0m,
                EmollientQualities = 76m,
                FoamBubble = 0m,
                FoamCreaminess = 18m,
                IodineNumber = 82m,
                Saponification = 0.1335m
            };
            oils.Add(avocado);
            Oil grape = new Oil
            {
                Name = "Масло виноградных косточек",
                Hardness = 11m,
                CleansingQualities = 0m,
                EmollientQualities = 88m,
                FoamBubble = 0m,
                FoamCreaminess = 11m,
                IodineNumber = 138m,
                Saponification = 0.1285m
            };
            oils.Add(grape);
            Oil jojoba = new Oil
            {
                Name = "Масло жожоба",
                Hardness = 1m,
                CleansingQualities = 0m,
                EmollientQualities = 9m,
                FoamBubble = 0m,
                FoamCreaminess = 1m,
                IodineNumber = 86m,
                Saponification = 0.0660m
            };
            oils.Add(jojoba);
            Oil wheatGerm = new Oil
            {
                Name = "Масло зародышей пшеницы",
                Hardness = 15m,
                CleansingQualities = 0m,
                EmollientQualities = 81m,
                FoamBubble = 0m,
                FoamCreaminess = 15m,
                IodineNumber = 131m,
                Saponification = 0.1310m
            };
            oils.Add(wheatGerm);
            Oil cocoa = new Oil
            {
                Name = "Масло какао",
                Hardness = 61m,
                CleansingQualities = 0m,
                EmollientQualities = 38m,
                FoamBubble = 0m,
                FoamCreaminess = 61m,
                IodineNumber = 37m,
                Saponification = 0.1380m
            };
            oils.Add(cocoa);
            Oil castor = new Oil
            {
                Name = "Касторовое масло",
                Hardness = 0m,
                CleansingQualities = 0m,
                EmollientQualities = 98m,
                FoamBubble = 90m,
                FoamCreaminess = 90m,
                IodineNumber = 86m,
                Saponification = 0.1286m
            };
            oils.Add(castor);
            Oil coconut = new Oil
            {
                Name = "Кокосовое масло",
                Hardness = 79m,
                CleansingQualities = 67m,
                EmollientQualities = 10m,
                FoamBubble = 67m,
                FoamCreaminess = 12m,
                IodineNumber = 10m,
                Saponification = 0.1830m
            };
            oils.Add(coconut);
            Oil macadamia = new Oil
            {
                Name = "Масло макадамии",
                Hardness = 12m,
                CleansingQualities = 0m,
                EmollientQualities = 61m,
                FoamBubble = 0m,
                FoamCreaminess = 12m,
                IodineNumber = 75m,
                Saponification = 0.1390m
            };
            oils.Add(macadamia);
            Oil almond = new Oil
            {
                Name = "Масло сладкого миндаля",
                Hardness = 7m,
                CleansingQualities = 0m,
                EmollientQualities = 89m,
                FoamBubble = 0m,
                FoamCreaminess = 7m,
                IodineNumber = 101m,
                Saponification = 0.1365m
            };
            oils.Add(almond);
            Oil olive = new Oil
            {
                Name = "Оливковое масло",
                Hardness = 17m,
                CleansingQualities = 0m,
                EmollientQualities = 82m,
                FoamBubble = 0m,
                FoamCreaminess = 17m,
                IodineNumber = 85m,
                Saponification = 0.1345m
            };
            oils.Add(olive);
            Oil palm = new Oil
            {
                Name = "Пальмовое масло",
                Hardness = 50m,
                CleansingQualities = 1m,
                EmollientQualities = 49m,
                FoamBubble = 1m,
                FoamCreaminess = 49m,
                IodineNumber = 53m,
                Saponification = 0.1405m
            };
            oils.Add(palm);
            Oil palmKernel = new Oil
            {
                Name = "Пальмоядровое масло",
                Hardness = 75m,
                CleansingQualities = 65m,
                EmollientQualities = 18m,
                FoamBubble = 65m,
                FoamCreaminess = 10m,
                IodineNumber = 20m,
                Saponification = 0.1762m
            };
            oils.Add(palmKernel);
            Oil peach = new Oil
            {
                Name = "Масло персиковых косточек",
                Hardness = 8m,
                CleansingQualities = 0m,
                EmollientQualities = 91m,
                FoamBubble = 0m,
                FoamCreaminess = 8m,
                IodineNumber = 101m,
                Saponification = 0.1345m
            };
            oils.Add(peach);
            Oil sunflower = new Oil
            {
                Name = "Подсолнечное масло",
                Hardness = 11m,
                CleansingQualities = 0m,
                EmollientQualities = 87m,
                FoamBubble = 0m,
                FoamCreaminess = 11m,
                IodineNumber = 133m,
                Saponification = 0.1350m
            };
            oils.Add(sunflower);
            Oil shea = new Oil
            {
                Name = "Масло ши",
                Hardness = 48m,
                CleansingQualities = 0m,
                EmollientQualities = 49m,
                FoamBubble = 0m,
                FoamCreaminess = 48m,
                IodineNumber = 58m,
                Saponification = 0.1284m
            };
            oils.Add(shea);

            return oils;
        }
    }
}