﻿using System;
using System.Text;


namespace LunarDust
{
    /// <summary>
    /// Класс AddToHtml служит для генерации кода html,
    /// отображающего слайдер и информацию о каждом экземпляре мыла,
    /// и последующего добавления этого кода в файл aspx.
    /// </summary>
    public static class AddToHtml
    {
        /// <summary>
        /// Метод AddDescriptionFull() генерирует и возвращает код html 
        /// (отображение полных описаний экземпляров мыла) в виде строки.
        /// </summary>
        /// <returns>Строка с кодом html.</returns>
        public static string AddDescriptionFull()
        {
            StringBuilder html = new StringBuilder();
            var soaps = ReadFromDb.ReadFromDbMethod();
            foreach (Soap soap in soaps)
            {
                html.Append(String.Format($@"<a href=""#slidelink"" id=""main{soap.Id}"" class=""main"">" +
                 $@"<div class=""composition""><h2>{soap.Name}</h2>{soap.Description_full}</div></a>"));        
            }
            return html.ToString();
        }

        /// <summary>
        /// Метод AddNameAndImage() генерирует и возвращает код html 
        /// (отображение слайдера с названиями, краткими описаниями 
        /// и изображениями экземпляров мыла) в виде строки.
        /// </summary>
        /// <returns>Строка с кодом html.</returns>
        public static string AddNameAndImage()
        {
            StringBuilder html = new StringBuilder();
            var soaps = ReadFromDb.ReadFromDbMethod();
            foreach (Soap soap in soaps)
            {
                html.Append(String.Format($@"<li class=""slide""><a href=""#main{soap.Id}"" class=""soapcaption"" target=""_self"">" +
                 $@"<p class=""soapname"">{soap.Name}</p> <p class=""soapdiscribe"">{soap.Description}" +
                 $@"</p></a><img class=""slideimg"" src=""data:image/jpg;base64,{soap.Image}"" alt=""""/></li>"));
            }
            return html.ToString();
        }

        /// <summary>
        /// Метод AddButton() генерирует и возвращает код html 
        /// (отображение кнопки для каждого экземплряра мыла в слайдере) в виде строки.
        /// </summary>
        /// <returns>Строка с кодом html.</returns>
        public static string AddButton()
        {
            StringBuilder html = new StringBuilder();
            var soaps = ReadFromDb.ReadFromDbMethod();
            foreach (Soap soap in soaps)
            {
                html.Append(@"<li class=""slidenavbtn""></li>");

            }
            return html.ToString();
        }
    }
}