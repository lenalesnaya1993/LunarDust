﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Drawing;

namespace LunarDust
{
    /// <summary>
    /// Класс SoapCalculator - CodeBehind страницы SoapCalculator.ascx
    /// (пользовательского контроллера "мыльный калькулятор")
    /// </summary>
    public partial class SoapCalculator : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        /// <summary>
        /// Получает список масел из класса OilsList
        /// </summary>
        readonly List<Oil> oils = OilsList.ReturnListOfOilsMethod();

        /// <summary>
        /// Метод SumOfOilsGrammsMethod рассчитывает сумму внесённых пользователем 
        /// грамм масел, выбранных для рассчёта формулы мыловарения, и возвращает сумму
        /// </summary>
        /// <returns>Сумма грамм масел.</returns>
        public decimal SumOfOilsGrammsMethod()
        {
            decimal sum = 0m;
            for (int i = 0; i < chooseOils.Items.Count; i++)
            {
                TextBox box = FindControl($"oilTextBox{i}") as TextBox;
                if (chooseOils.Items[i].Selected && box.Text != "") 
                {
                    sum += Convert.ToDecimal(box.Text);
                }
            }
            return sum;
        }

        /// <summary>
        /// Метод PercentsAndGrammsOfOilsMethod рассчитывает проценты внесённых пользователем 
        /// грамм масел, выбранных для рассчёта формулы мыловарения, отображает проценты и граммы масел
        /// </summary>
        /// <param name="sum">Общий вес выбранных масел</param>
        public void PercentsAndGrammsOfOilsMethod(decimal sum)
        {
            for (int i = 0; i < chooseOils.Items.Count; i++)
            {
                TextBox box = FindControl($"oilTextBox{i}") as TextBox;
                if (chooseOils.Items[i].Selected && box.Text != "" && box.Text != "0")
                {
                    decimal oilValue = Math.Round(Convert.ToDecimal(box.Text), 2);
                    decimal percent = Math.Round(oilValue / (sum / 100m));
                    (FindControl($"oilWeight{i}") as Label).Text = $"{oilValue} г."; 
                    (FindControl($"oilPercent{i}") as Label).Text = $"{percent} %";
                }
                if (box.Text == "" || box.Text == "0")
                {
                    (FindControl($"oilWeight{i}") as Label).Text = "0 г.";
                    (FindControl($"oilPercent{i}") as Label).Text = "%";
                }
            }
        }

        /// <summary>
        /// Метод LyeSumMethod рассчитывает, отображает и возвращает количество щёлочи 
        /// для рецепта мыловарения (в зависимости от набора и количества масел)
        /// </summary>
        /// <returns>Количество щёлочи.</returns>
        public decimal LyeSumMethod()
        {
            decimal lye = 0m;
            decimal oilValue;
           
            decimal percent = Convert.ToDecimal(DropdownOil.SelectedValue);

            for (int i = 0; i < chooseOils.Items.Count; i++)
            {
                TextBox box = FindControl($"oilTextBox{i}") as TextBox;
                if (chooseOils.Items[i].Selected && box.Text != "")
                {
                    oilValue = Convert.ToDecimal(box.Text);
                    lye += oilValue / 100m * percent * oils[i].Saponification; 
                }
            }
            lyelabel.Text = $"{Math.Round(lye, 2)} г.";
            return lye;
        }

        /// <summary>
        /// Метод PercentsOfOverfatAndWaterMethod отображает процент воды 
        /// и процент свободных жиров
        /// </summary>
        public void PercentsOfOverfatAndWaterMethod()
        {
            decimal water = Convert.ToDecimal(DropdownWater.SelectedValue);
            waterpercentlabel.Text = $"{Math.Round(water)} %";
            decimal oil = 100m - Convert.ToDecimal(DropdownOil.SelectedValue);
            oilpercentlabel.Text = $"{Math.Round(oil)} %";
        }

        /// <summary>
        /// Метод BalanceNumbersMethod служит для расчёта и отображения
        /// сбалансированности получившегося рецепта мыловарения
        /// </summary>
        public void BalanceNumbersMethod()
        {
            decimal sumOfOils = 0m;
            decimal gHardness = 0m;
            decimal gCleansingQualities = 0m;
            decimal gEmollientQualities = 0m;
            decimal gFoamBubble = 0m;
            decimal gFoamCreaminess = 0m;
            decimal gIodineNumber = 0m;

            decimal h, c, e, fb, fc, io;
            int check = 0;
            for (int i = 0; i < chooseOils.Items.Count; i++)
            {
                TextBox box = FindControl($"oilTextBox{i}") as TextBox;
                decimal oilValue;
                if (chooseOils.Items[i].Selected && box.Text != "" && box.Text != "0")
                {
                    check++;
                    oilValue = Convert.ToDecimal(box.Text);
                    sumOfOils += oilValue;
                    gHardness += oils[i].Hardness * oilValue;
                    gCleansingQualities += oils[i].CleansingQualities * oilValue;
                    gEmollientQualities += oils[i].EmollientQualities * oilValue;
                    gFoamBubble += oils[i].FoamBubble * oilValue;
                    gFoamCreaminess += oils[i].FoamCreaminess * oilValue;
                    gIodineNumber += oils[i].IodineNumber * oilValue;
                }
            }

            if (check == 0) 
            {
                hardnesslabel.Text = "0";
                hardnesslabel.ForeColor = Color.DimGray;
                cleansinglabel.Text = "0";
                cleansinglabel.ForeColor = Color.DimGray;
                emollientlabel.Text = "0";
                emollientlabel.ForeColor = Color.DimGray;
                bubblelabel.Text = "0";
                bubblelabel.ForeColor = Color.DimGray;
                creaminesslabel.Text = "0";
                creaminesslabel.ForeColor = Color.DimGray;
                iodlabel.Text = "0";
                iodlabel.ForeColor = Color.DimGray;
                return; 
            }

            h = Math.Round(gHardness / sumOfOils);
            c = Math.Round(gCleansingQualities / sumOfOils);
            e = Math.Round(gEmollientQualities / sumOfOils);
            fb = Math.Round(gFoamBubble / sumOfOils);
            fc = Math.Round(gFoamCreaminess / sumOfOils);
            io = Math.Round(gIodineNumber / sumOfOils);

            hardnesslabel.Text = $"{h}";
            cleansinglabel.Text = $"{c}";
            emollientlabel.Text = $"{e}";
            bubblelabel.Text = $"{fb}";
            creaminesslabel.Text = $"{fc}";
            iodlabel.Text = $"{io}";

            if (h < 25m || h > 55m) hardnesslabel.ForeColor = Color.Red;
            else if (h >= 35m && h <= 45m) hardnesslabel.ForeColor = Color.Green;
            else hardnesslabel.ForeColor = Color.Orange;

            if (c < 7m || c > 30m) cleansinglabel.ForeColor = Color.Red;
            else if (c >= 15m && c <= 20m) cleansinglabel.ForeColor = Color.Green;
            else cleansinglabel.ForeColor = Color.Orange;

            if (e < 40m || e > 80m) emollientlabel.ForeColor = Color.Red;
            else if (e >= 50m && e <= 70m) emollientlabel.ForeColor = Color.Green;
            else emollientlabel.ForeColor = Color.Orange;

            if (fb < 7m || fb > 40m) bubblelabel.ForeColor = Color.Red;
            else if (fb >= 15m && fb <= 30m) bubblelabel.ForeColor = Color.Green;
            else bubblelabel.ForeColor = Color.Orange;

            if (fc < 7m || fc > 45m) creaminesslabel.ForeColor = Color.Red;
            else if (fc >= 15m && fc <= 3m) creaminesslabel.ForeColor = Color.Green;
            else creaminesslabel.ForeColor = Color.Orange;

            if (io < 0m || io > 70m) iodlabel.ForeColor = Color.Red;
            else if (io >= 0m && io <= 60m) iodlabel.ForeColor = Color.Green;
            else iodlabel.ForeColor = Color.Orange;
        }

        /// <summary>
        /// Метод CalcButton_Click - обработчик события Click кнопки
        /// CalcButton, служит для рассчёта и отображения рецепта мыловарения
        /// и сбалансированности рецепта
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CalcButton_Click(object sender, EventArgs e)
        {
            try
            {
                decimal oilsSum = SumOfOilsGrammsMethod();
                PercentsAndGrammsOfOilsMethod(oilsSum);
                decimal lyeSum = LyeSumMethod();
                decimal water = oilsSum / 100m * Convert.ToDecimal(DropdownWater.SelectedValue);
                waterlabel.Text = $"{Math.Round(water, 2)} г.";
                PercentsOfOverfatAndWaterMethod();
                decimal generalWeight = oilsSum + lyeSum + water;
                weightlabel.Text = $"{Math.Round(generalWeight, 2)} г.";
                BalanceNumbersMethod();
            }
            catch { }
        }

        /// <summary>
        /// Метод SaveOilsButton_Click - обработчик события кнопки 
        /// saveOils, служит для отображения названий выбранных масел 
        /// и текстовых полей для ввода грамм каждого из масел, 
        /// а также для корректного отображения рецепта и сбалансированности 
        /// при смене масел
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveOilsButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < chooseOils.Items.Count; i++)
            {
                if (chooseOils.Items[i].Selected)
                {
                    Label oilLabel = FindControl($"oilLabel{i}") as Label;
                    oilLabel.Text = oils[i].Name;
                    oilLabel.Visible = true;

                    TextBox oilTextBox = FindControl($"oilTextBox{i}") as TextBox;
                    oilTextBox.Visible = true;
                    
                    Label g = FindControl($"g{i}") as Label;
                    g.Visible = true;
                    

                    Label oilSecondLabel = FindControl($"oilSecondLabel{i}") as Label;
                    oilSecondLabel.Text = oils[i].Name;
                    oilSecondLabel.Visible = true;

                    Label oilWeight = FindControl($"oilWeight{i}") as Label;
                    oilWeight.Visible = true;

                    Label oilPercent = FindControl($"oilPercent{i}") as Label;
                    oilPercent.Visible = true;

                }
                if (!chooseOils.Items[i].Selected)
                {
                    Label oilLabel = FindControl($"oilLabel{i}") as Label;
                    oilLabel.Visible = false;

                    TextBox oilTextBox = FindControl($"oilTextBox{i}") as TextBox;
                    oilTextBox.Visible = false;
                    Label g = FindControl($"g{i}") as Label;
                    g.Visible = false;

                    Label oilSecondLabel = FindControl($"oilSecondLabel{i}") as Label;
                    oilSecondLabel.Visible = false;

                    Label oilWeight = FindControl($"oilWeight{i}") as Label;
                    oilWeight.Visible = false;

                    Label oilPercent = FindControl($"oilPercent{i}") as Label;
                    oilPercent.Visible = false;
                }
            }
            try
            {
                decimal oilsSum = SumOfOilsGrammsMethod();
                PercentsAndGrammsOfOilsMethod(oilsSum);
                decimal lyeSum = LyeSumMethod();
                decimal water = oilsSum / 100m * Convert.ToDecimal(DropdownWater.SelectedValue);
                waterlabel.Text = $"{Math.Round(water, 2)} г.";
                PercentsOfOverfatAndWaterMethod();
                decimal generalWeight = oilsSum + lyeSum + water;
                weightlabel.Text = $"{Math.Round(generalWeight, 2)} г.";
                BalanceNumbersMethod();
            }
            catch { }
        }
    }
}