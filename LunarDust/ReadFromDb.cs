﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;


namespace LunarDust
{

    /// <summary>
    /// Класс ReadFromDb служит для считывания всех строк из таблицы Soap_information
    /// базы данных Soap
    /// </summary>
    public static class ReadFromDb
    {
        /// <summary>
        /// Метод ReadFromDbMethod служит для считывания всех строк из таблицы Soap_information
        /// базы данных Soap. Создаёт список экземпляров мыла (класса Soap), содержащий информацию,
        /// прочитанную из таблицы Soap_information, и возвращает список.
        /// </summary>
        /// <returns>Список экземпляров мыла (класса Soap).</returns>
        public static List<Soap> ReadFromDbMethod()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            List<Soap> soaps = new List<Soap>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = "SELECT * FROM Soap_information";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();

                while(reader.Read())
                {
                    // при случае getordinal
                    int id = reader.GetInt32(0);
                    string name = reader.GetString(1);
                    string description = reader.GetString(2);
                    string description_full = reader.GetString(3);
                    byte[] image = (byte[])reader.GetValue(4);

                    Soap soap = new Soap(id, name, description, description_full, image);
                    soaps.Add(soap);
                }
                reader.Dispose(); // Close()          
            }
            return soaps;
        }
    }
}