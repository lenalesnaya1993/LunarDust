﻿<%@ Page Title="Home Page" Language="C#"  AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="LunarDust.Default" %>
<%@ Import Namespace="LunarDust" %>
<%@ Register Src="~/SoapCalculator.ascx" TagName="CalcControl" TagPrefix="TCalcControl" %>


<!DOCTYPE HTML>
<html lang = "ru">
<head runat="server">
	<meta charset = "utf-8">
	<meta name = "description" content="GuestsPage">
	<meta name = "keywords" content="ремесло, ремесленная продукция, рукоделие, ручная работа, 
		мыло ручной работы, крафтовое мыло, натуральное мыло, растительные масла, эфирные масла, craft, 
		handicraft products, handcraft, handmade, handmade soap, craft soap, natural soap, vegetable oils, essential oils">
	<meta name = "robots" content="index, follow">
	<title>MainPage</title>
	<link rel = "stylesheet" href="Content/reset.css">
	<link rel = "stylesheet" href="Content/style.css">
	<link rel = "shortcut icon" href="img/little.png" type="image/png">
</head>
	<body>	
	<form runat="server">
		<header id="mainheader">
			<nav>
				<ul id="headerul">
					<li>
						<img class='logo' src="img/lunarfactory.png" alt ="LunarDust"><br>
						<img class='logocaption' src="img/lundustcaption2.png" alt ="натуральное мыло">
					</li>
					<li><a href="#pict1" target="_self">о нас</a></li>
					<li><a href="#pict2" target="_self">калькулятор</a></li>
					<li><a href=".contactref" target="_self">контакты</a></li>
				</ul>
			</nav> 		
		</header>
		<div id="mainpart">
			<a id="slidelink"/>
			<div  id="positionfixed" class="width"><a href="#mainheader"  target="_self" title="Вверх"><div><img src="img/arrow.png"/></div></a></div>
			<%=  AddToHtml.AddDescriptionFull() %>						
		<div id="sliderblock">
				<div id="viewport">
					<ul id="slidewrap">
			<%= AddToHtml.AddNameAndImage() %>
					</ul>
					<div id="prevnextbtns">
						<div id="prevbtn"></div>
						<div id="nextbtn"></div>
					</div>
					<ul id="navbtns">
			<%= AddToHtml.AddButton() %>					
					</ul>
				</div>
			</div>
			<img id="pict1" class="picturegreen" src="img/picturegreen.png" alt ="pict">
			<div id="aboutus">
				<article> <h2>О нас</h2><p>Мы с мамой создаём натуральное мыло у неё на кухне. Такое у нас хобби. 
				Изготавливаем мы его полностью с нуля. Что это значит? Мы берём натриевую щёлочь (она же каустическая сода, 
				она же едкий натр, она же гидроокись натрия, она же NaOH), растворяем её в воде, полученный раствор смешиваем 
				с растительными маслами в особой пропорции (при этом масла так сказать «омыляются») и получается такая кашица – 
				основа для мыла. Об омылении жиров щёлочью и особых свойствах такого сочетания человечеству стало известно 
				давныым-давно, ещё до нашей эры. Раньше в качестве источника щёлочи использовали древесную золу. А далее мы 
				сдабриваем это дело полезными кислотами, ухаживающими растительными и эфирными маслами, глинами, травяными 
				настоями и мацератами и оставляем «созревать» в форме либо варим в духовке. </p><p>Ингридиенты для мыла, такие как 
				растительные и эфирные масла, щёлочь, кислоты и глины мы добываем в магазине товаров для творчества <a href="http://homeart.by/" 
				target="_blank" title="Перейти"> homeart.by </a>. Травы для настоев и мацератов мы собираем сами.  
				А приобрести уже готовое и упакованное наше мыло вы можете в магазине натуральной косметики <a href="http://www.igreen.by/" 
				target="_blank" title="Перейти"> iGreen.by </a>. Эти магазины 
				находятся в г. Минске, но у них есть доставка по всей Беларуси. </p></article>
			</div>
			<img id="pict2" class="picturegreen" src="img/picturegreen.png" alt ="pict">
			<TCalcControl:CalcControl ID="Calculator" runat="server" />
			<div class="contactref"></div>
			<div class="contaсt"> 
				<img class="messenger" src="img/telegram.png" alt ="telegram">
				<img class="messenger" src="img/viber.png" alt ="viber">
				<h2>+375(33)6386160<br> - напиши нам</h2>
			</div>
		<div class="width"><a href="#login" id="closewindow" target="_self" title="Войти"><div class="enterlink"><img src="img/door.png"/></div></a></div>
		<div id="login"><div class="loginbox"><a href="#closewindow" class="closeref" target="_self">X</a><p>Введите ключ авторизации</p>
			<asp:TextBox ID="keyTextBox" class="textbox" Text="" runat="server" BackColor="White" BorderColor="#999999" BorderStyle="Solid" CausesValidation="False" ForeColor="#666666" BorderWidth="1" TextMode="Password"/>
            <asp:Button ID="loginButton" runat="server" Text="Войти" OnClick="LoginButtonEventMethod" UseSubmitBehavior="True" ValidateRequestMode="Inherit" CssClass="controlsmargin" />
            <div><asp:Label ID="errorLabel" runat="server" Text="" Font-Size="12px" ForeColor="Red" CssClass="controlsmargin"></asp:Label></div></div></div>
		</div>
		<footer>
			<figure class="footerlink"><a href="http://www.igreen.by/" target="_blank" title="Перейти"> <img src="img/igreen.png" alt="iGreen.by"> </a></figure>
			<figure class="footerlink"><a href="http://homeart.by/" target="_blank" title="Перейти"> <img src="img/homeart.png" alt="homeart.png"> </a></figure>
		</footer> 
	<script src="Scripts/jquery.js"></script>
	<script src="Scripts/script.js"></script>
	</form>
	</body>
</html>
