$(document).ready(function () {
	// код для плавного перехода взят с сайта https://webcomplex.com.ua/jquery/plavnyj-skroll-posle-nazhatiya-na-yakornuyu-ssylku.html	
	$("#headerul").on("click", "a", function (event) {
		//отменяем стандартную обработку нажатия по ссылке
		event.preventDefault();
		var id = $(this).attr('href'),
			top = $(id).offset().top;
		//анимируем переход на расстояние - top за 500 мс
		$('html').animate({ scrollTop: top }, 500);
	});

	$("#positionfixed").on("click", "a", function (event) {
		//отменяем стандартную обработку нажатия по ссылке
		event.preventDefault();
		var id = $(this).attr('href'),
			top = $(id).offset().top;
		//анимируем переход на расстояние - top за 500 мс
		$('html').animate({ scrollTop: top }, 500);
	});

	// код для слайдера взят с сайта https://habr.com/ru/post/319394/
	var slideNow = 1;
	var slideCount = $('#slidewrap').children().length;
	var translateWidth = 0;
	//var slideInterval = 5000;

	function nextSlide() {
		if (slideNow == slideCount || slideNow <= 0 || slideNow > slideCount) {
			$('#slidewrap').css('transform', 'translate(0, 0)');
			slideNow = 1;
		} else {
			translateWidth = -$('#viewport').width() * (slideNow);
			$('#slidewrap').css({
				'transform': 'translate(' + translateWidth + 'px, 0)',
				'-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
				'-moz-transform': 'translate(' + translateWidth + 'px, 0)',
			});
			slideNow++;
		}
	}

	function prevSlide() {
		if (slideNow == 1 || slideNow <= 0 || slideNow > slideCount) {
			translateWidth = -$('#viewport').width() * (slideCount - 1);
			$('#slidewrap').css({
				'transform': 'translate(' + translateWidth + 'px, 0)',
				'-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
				'-moz-transform': 'translate(' + translateWidth + 'px, 0)',
			});
			slideNow = slideCount;
		} else {
			translateWidth = -$('#viewport').width() * (slideNow - 2);
			$('#slidewrap').css({
				'transform': 'translate(' + translateWidth + 'px, 0)',
				'-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
				'-moz-transform': 'translate(' + translateWidth + 'px, 0)',
			});
			slideNow--;
		}
	}

	var navBtnId = 0;
	$('.slidenavbtn').click(function () {
		navBtnId = $(this).index();

		if (navBtnId + 1 != slideNow) {
			translateWidth = -$('#viewport').width() * (navBtnId);
			$('#slidewrap').css({
				'transform': 'translate(' + translateWidth + 'px, 0)',
				'-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
				'-moz-transform': 'translate(' + translateWidth + 'px, 0)',
			});
			slideNow = navBtnId + 1;
		}
	});

	// var switchInterval = setInterval(nextSlide, slideInterval);

	// $('#viewport').hover(function(){
	// clearInterval(switchInterval);},
	// function() {
	// switchInterval = setInterval(nextSlide, slideInterval);
	// });

	$('#nextbtn').click(function () {
		nextSlide();
	});

	$('#prevbtn').click(function () {
		prevSlide();
	});

	$('#calculator').keypress(function NumericText() {
		var key = window.event.keyCode;
		if (key < 48 || key > 57)
			window.event.returnValue = false;
	});

	$('#calculator').on("cut copy paste", function (e) {
		e.preventDefault();
	});

});
