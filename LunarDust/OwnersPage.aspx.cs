﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;


namespace LunarDust
{
    /// <summary>
    /// Класс OwnersPage - CodeBehind страницы OwnersPage.aspx
    /// (страницы авторизованного пользователя)
    /// </summary>
    public partial class OwnersPage : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            sendInfoLabel.Text = "";
            deleteLabel.Text = "";
            alterLabel.Text = "";
            notFoundNameLabel.Text = "";
        }

        /// <summary>
        /// Получает строку подключения к базе данных Soap из файла Web.config
        /// </summary>
        static readonly string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        /// <summary>
        /// Создаёт подключение к базе данных Soap.
        /// </summary>
        readonly SqlConnection connection = new SqlConnection(connectionString);

        /// <summary>
        /// Метод AddPositionEventMethod - обработчик события Click
        /// кнопки sendButton, служит для добавления новой строки в таблицу Soap_information
        /// базы данных Soap
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AddPositionEventMethod(object sender, EventArgs e)
        {
            string name;
            if (String.IsNullOrWhiteSpace(nameTextBox.Text)) name = null;
            else name = nameTextBox.Text;

            string description = descriptionTextBox.Text;
            string descriptionFull = description_fullTextBox.Text;

            byte[] image;
            if (ImageName.FileBytes.Any()) image = ImageName.FileBytes;
            else image = null;

            try
                {
                 connection.Open();
                 SqlCommand command = new SqlCommand();
                 command.Connection = connection;
                 command.CommandText = @"INSERT INTO Soap_information VALUES (@Soap_name, @Soap_description, @Soap_description_full, @Soap_image)";

                 SqlParameter nameParam = new SqlParameter("@Soap_name", name);
                 command.Parameters.Add(nameParam);
                 SqlParameter descriptionParam = new SqlParameter("@Soap_description", description);
                 command.Parameters.Add(descriptionParam);
                 SqlParameter descriptionFullParam = new SqlParameter("@Soap_description_full", descriptionFull);
                 command.Parameters.Add(descriptionFullParam);
                 SqlParameter imageParam = new SqlParameter("@Soap_image", image);
                 command.Parameters.Add(imageParam);

                 command.ExecuteNonQuery();
                 sendInfoLabel.Text = "Сохранено успешно";
                 nameTextBox.Text = "";
                 descriptionTextBox.Text = "";
                 description_fullTextBox.Text = "";
                }
            catch
                {
                 if ((name == null) || (image == null))
                     sendInfoLabel.Text = "Название позиции и изображение являются обязательными атрибутами";
                 else sendInfoLabel.Text = "Позиция с таким именем уже есть в каталоге";
                }
            finally
                {
                 connection.Dispose(); //Close()
                }
            }

        /// <summary>
        /// Метод DeletePositionEventMethod - обработчик события Click
        /// кнопки deleteButton, служит для удаления строки из таблицы Soap_information
        /// базы данных Soap
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DeletePositionEventMethod(object sender, EventArgs e)
        {
            try
                {
                 connection.Open();
                 SqlCommand command = new SqlCommand();
                 command.Connection = connection;
                 command.CommandText = @"DELETE FROM Soap_information WHERE Soap_name = '" + DeleteNameTextBox.Text + "'";
                 int strings = command.ExecuteNonQuery();

                 if (strings == 0) deleteLabel.Text = "Позиции с таким именем нет в каталоге";
                 else { 
                     deleteLabel.Text = "Удалено успешно";
                     DeleteNameTextBox.Text = "";
                      }
                }
            catch 
                {
                 deleteLabel.Text = "Ошибка!";
                }
            finally
                {
                 connection.Dispose(); // Close()
                }
        }

        /// <summary>
        /// Служит для хранения данных файла изображения прочитанной из базы данных Soap строки,
        /// выбранной для редактирования
        /// </summary>
        static string imageString = string.Empty;

        /// <summary>
        /// Метод ReadByNameEventMethod - обработчик события Click
        /// кнопки readByNameButton, служит для поиска и отображения строки 
        /// из таблицы Soap_information базы данных Soap
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ReadByNameEventMethod(object sender, EventArgs e)
        {
            try
                {
                 connection.Open();
                 string sql = "SELECT * FROM Soap_information WHERE Soap_name = '" + readByNameTextBox.Text + "'";
                 SqlCommand command = new SqlCommand(sql, connection);
                 SqlDataReader reader = command.ExecuteReader();
                 if (reader.HasRows)
                   {
                     while (reader.Read())
                       {
                         // getordinal при случае
                         IDLabel.Text = Convert.ToString(reader.GetInt32(0));
                         alterNameTextBox.Text = reader.GetString(1);
                         alterDescTextBox.Text = reader.GetString(2);
                         alterDescFullTextBox.Text = reader.GetString(3);
                         byte[] imageData = (byte[])reader.GetValue(4);

                         imageString = Convert.ToBase64String(imageData);
                        }
                     reader.Dispose(); // Close()
                    }
                 else notFoundNameLabel.Text = "Позиции с таким именем нет в каталоге";
                }
            catch 
                {
                 notFoundNameLabel.Text = "Ошибка!";
                }
            finally
                {
                 connection.Dispose(); // Close()
                }
            }

        /// <summary>
        /// Метод AlterPositionEventMethod - обработчик события Click
        /// кнопки alterButton, служит для редактирования найденной в методе 
        /// ReadByNameEventMethod строки в таблице Soap_information базы данных Soap
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AlterPositionEventMethod(object sender, EventArgs e)
        {
            string name;
            if (String.IsNullOrWhiteSpace(alterNameTextBox.Text)) name = null;
            else name = alterNameTextBox.Text;

            byte[] image;
            if (ImageAltData.FileBytes.Any()) image = ImageAltData.FileBytes;
            else image = Convert.FromBase64String(imageString);

            try
                {
                 connection.Open();
                 SqlCommand command = new SqlCommand();
                 command.Connection = connection;

                 command.CommandText = @"UPDATE Soap_information SET Soap_name = @Soap_name" +
                     ", Soap_description = @Soap_description, Soap_description_full = @Soap_description_full" +
                     ", Soap_image = @Soap_image WHERE Soap_id = " + IDLabel.Text;

                 SqlParameter nameParam = new SqlParameter("@Soap_name", name);
                 command.Parameters.Add(nameParam);
                 SqlParameter descriptionParam = new SqlParameter("@Soap_description", alterDescTextBox.Text);
                 command.Parameters.Add(descriptionParam);
                 SqlParameter descriptionFullParam = new SqlParameter("@Soap_description_full", alterDescFullTextBox.Text);
                 command.Parameters.Add(descriptionFullParam);
                 SqlParameter imageParam = new SqlParameter("@Soap_image", image);
                 command.Parameters.Add(imageParam);

                 command.ExecuteNonQuery();
                 alterLabel.Text = "Изменения сохранены";
                 IDLabel.Text = "";
                 readByNameTextBox.Text = "";
                 alterNameTextBox.Text = "";
                 alterDescTextBox.Text= "";
                 alterDescFullTextBox.Text = "";
                }
            catch
                {
                 if (String.IsNullOrWhiteSpace(IDLabel.Text)) alterLabel.Text = "Не выбрана позиция для редактирования";
                 else if (name == null) alterLabel.Text = "Имя позиции является обязательным атрибутом";
                 else alterLabel.Text = "Позиция с таким именем уже есть в каталоге";
                }
            finally
                {
                 connection.Dispose(); // Close()
                }
        }
    }
}