﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI;


namespace LunarDust
{
    /// <summary>
    /// Класс Default - CodeBehind страницы Default.aspx
    /// (основной страницы сайта)
    /// </summary>
    public partial class Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            errorLabel.Text = "";
        }

        /// <summary>
        /// Метод LoginButtonEventMethod - обработчик события 
        /// Click кнопки LoginButton. Осуществляет проверку введённого пользователем ключа авторизации
        /// и в случае верности введённого ключа перенаправляет на страницу авторизованного пользователя.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void LoginButtonEventMethod (object sender, EventArgs e)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string password = null;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = "SELECT * FROM Owners_password";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();

                reader.Read();
                password = reader.GetString(0);

                reader.Dispose(); //Close()
            }

            if (password == null) errorLabel.Text = "Неполадки на сервере, попробуйте позже";
            else if (Equals(password, keyTextBox.Text)) 
            {
                Response.Redirect("OwnersPage.aspx"); 
            }
            else errorLabel.Text = "Неверный ключ авторизации!"; 
        }
    }
}