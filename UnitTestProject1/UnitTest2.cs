﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LunarDust;
using System.Text.RegularExpressions;

namespace UnitTestProject1
{
    [TestClass]
    public class AddToHtmlUnitTest
    {
        [TestMethod]
        public void AddDescriptionFullTestMethod1()
        {
            string html = AddToHtml.AddDescriptionFull();
            string word = "#slidelink";

            Regex rgx = new Regex(word, RegexOptions.IgnoreCase);
            MatchCollection counter = rgx.Matches(html);

            Assert.AreEqual(counter.Count, 8);
        }

        [TestMethod]
        public void AddDescriptionFullTestMethod2()
        {
            string html = AddToHtml.AddDescriptionFull();
            Assert.IsTrue(html.Contains("..Сосновое.."));
            Assert.IsTrue(html.Contains("..Овсяно-медовое.."));
            Assert.IsTrue(html.Contains("..Еловое.."));
            Assert.IsTrue(html.Contains("..Розовое.."));
            Assert.IsTrue(html.Contains("..Шампуневое.."));
            Assert.IsTrue(html.Contains("..Шоколадное.."));
            Assert.IsTrue(html.Contains("..Соляное.."));
            Assert.IsTrue(html.Contains("..Глиняное.."));
        }

        [TestMethod]
        public void AddNameAndImageTestMethod1()
        {
            string html = AddToHtml.AddNameAndImage();
            string word = "#main";

            Regex rgx = new Regex(word, RegexOptions.IgnoreCase);
            MatchCollection counter = rgx.Matches(html);

            Assert.AreEqual(counter.Count, 8);
        }

        [TestMethod]
        public void AddNameAndImageTestMethod2()
        {
            string html = AddToHtml.AddNameAndImage();
            Assert.IsTrue(html.Contains("..Сосновое.."));
            Assert.IsTrue(html.Contains("..Овсяно-медовое.."));
            Assert.IsTrue(html.Contains("..Еловое.."));
            Assert.IsTrue(html.Contains("..Розовое.."));
            Assert.IsTrue(html.Contains("..Шампуневое.."));
            Assert.IsTrue(html.Contains("..Шоколадное.."));
            Assert.IsTrue(html.Contains("..Соляное.."));
            Assert.IsTrue(html.Contains("..Глиняное.."));
        }

        [TestMethod]
        public void AddButtonTestMethod1()
        {
            string html = AddToHtml.AddButton();
            string word = "slidenavbtn";

            Regex rgx = new Regex(word, RegexOptions.IgnoreCase);
            MatchCollection counter = rgx.Matches(html);

            Assert.AreEqual(counter.Count, 8);
        }
    }
}
