﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LunarDust;
using System.Data.SqlClient;

namespace UnitTestProject1
{
    [TestClass]
    public class ReadFromDbUnitTest
    {
        [TestMethod]
        public void ReadFromDbMethodTest1()
        {
            List<Soap> soaps = ReadFromDb.ReadFromDbMethod();
            Assert.AreEqual(soaps.Count, 8); //считаем количество строк, переданных из базы данных
        }

        [TestMethod]
        public void ReadFromDbMethodTest2()
        {
            List<Soap> soaps = ReadFromDb.ReadFromDbMethod();
            Assert.IsTrue(soaps[0].Name.Trim() == "..Сосновое..");
            Assert.IsTrue(soaps[1].Name.Trim() == "..Овсяно-медовое..");
            Assert.IsTrue(soaps[2].Name.Trim() == "..Еловое..");
            Assert.IsTrue(soaps[3].Name.Trim() == "..Розовое..");
            Assert.IsTrue(soaps[4].Name.Trim() == "..Шампуневое..");
            Assert.IsTrue(soaps[5].Name.Trim() == "..Шоколадное..");
            Assert.IsTrue(soaps[6].Name.Trim() == "..Соляное..");
            Assert.IsTrue(soaps[7].Name.Trim() == "..Глиняное..");
        }

        [TestMethod]
        public void ReadFromDbMethodTest3()
        {
            List<Soap> soaps = ReadFromDb.ReadFromDbMethod();
            Assert.IsTrue(soaps[0].Description.Trim() == "для нормальной кожи");
            Assert.IsTrue(soaps[1].Description.Trim() == "для нормальной и сухой кожи");
            Assert.IsTrue(soaps[2].Description.Trim() == "для жирной кожи");
            Assert.IsTrue(soaps[3].Description.Trim() == "для нормальной и сухой кожи");
            Assert.IsTrue(soaps[4].Description.Trim() == "для всех типов волос");
            Assert.IsTrue(soaps[5].Description.Trim() == "для нормальной и сухой кожи");
            Assert.IsTrue(soaps[6].Description.Trim() == "для нормальной и проблемной кожи");
            Assert.IsTrue(soaps[7].Description.Trim() == "для нормальной кожи");
        }

        [TestMethod]
        public void ReadFromDbMethodTest4()
        {
            List<Soap> soaps = ReadFromDb.ReadFromDbMethod();
            string connectionString = @"Data Source=DESKTOP-30OVRQL\SQLEXPRESS;Initial Catalog=Soap;Integrated Security=True";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                string sql = "SELECT * FROM Soap_information WHERE Soap_name = '..Сосновое..'";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                string description = reader.GetString(reader.GetOrdinal("Soap_description_full"));
                Assert.AreEqual(soaps[0].Description_full, description);
                reader.Close();

                sql = "SELECT * FROM Soap_information WHERE Soap_name = '..Овсяно-медовое..'";
                command = new SqlCommand(sql, connection);
                reader = command.ExecuteReader();
                reader.Read();
                description = reader.GetString(reader.GetOrdinal("Soap_description_full"));
                Assert.AreEqual(soaps[1].Description_full, description);
                reader.Close();

                sql = "SELECT * FROM Soap_information WHERE Soap_name = '..Еловое..'";
                command = new SqlCommand(sql, connection);
                reader = command.ExecuteReader();
                reader.Read();
                description = reader.GetString(reader.GetOrdinal("Soap_description_full"));
                Assert.AreEqual(soaps[2].Description_full, description);
                reader.Close();

                sql = "SELECT * FROM Soap_information WHERE Soap_name = '..Розовое..'";
                command = new SqlCommand(sql, connection);
                reader = command.ExecuteReader();
                reader.Read();
                description = reader.GetString(reader.GetOrdinal("Soap_description_full"));
                Assert.AreEqual(soaps[3].Description_full, description);
                reader.Close();

                sql = "SELECT * FROM Soap_information WHERE Soap_name = '..Шампуневое..'";
                command = new SqlCommand(sql, connection);
                reader = command.ExecuteReader();
                reader.Read();
                description = reader.GetString(reader.GetOrdinal("Soap_description_full"));
                Assert.AreEqual(soaps[4].Description_full, description);
                reader.Close();

                sql = "SELECT * FROM Soap_information WHERE Soap_name = '..Шоколадное..'";
                command = new SqlCommand(sql, connection);
                reader = command.ExecuteReader();
                reader.Read();
                description = reader.GetString(reader.GetOrdinal("Soap_description_full"));
                Assert.AreEqual(soaps[5].Description_full, description);
                reader.Close();

                sql = "SELECT * FROM Soap_information WHERE Soap_name = '..Соляное..'";
                command = new SqlCommand(sql, connection);
                reader = command.ExecuteReader();
                reader.Read();
                description = reader.GetString(reader.GetOrdinal("Soap_description_full"));
                Assert.AreEqual(soaps[6].Description_full, description);
                reader.Close();

                sql = "SELECT * FROM Soap_information WHERE Soap_name = '..Глиняное..'";
                command = new SqlCommand(sql, connection);
                reader = command.ExecuteReader();
                reader.Read();
                description = reader.GetString(reader.GetOrdinal("Soap_description_full"));
                Assert.AreEqual(soaps[7].Description_full, description);
                reader.Close();

                connection.Close();
            }
        }

        [TestMethod]
        public void ReadFromDbMethodTest5()
        {
            List<Soap> soaps = ReadFromDb.ReadFromDbMethod();
            string connectionString = @"Data Source=DESKTOP-30OVRQL\SQLEXPRESS;Initial Catalog=Soap;Integrated Security=True";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                string sql = "SELECT * FROM Soap_information WHERE Soap_name = '..Сосновое..'";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                byte[] imageData = (byte[])reader.GetValue(reader.GetOrdinal("Soap_image"));
                Assert.AreEqual(soaps[0].Image, Convert.ToBase64String(imageData));
                reader.Close(); 

                sql = "SELECT * FROM Soap_information WHERE Soap_name = '..Овсяно-медовое..'";
                command = new SqlCommand(sql, connection);
                reader = command.ExecuteReader();
                reader.Read();
                imageData = (byte[])reader.GetValue(reader.GetOrdinal("Soap_image"));
                Assert.AreEqual(soaps[1].Image, Convert.ToBase64String(imageData));
                reader.Close();

                sql = "SELECT * FROM Soap_information WHERE Soap_name = '..Еловое..'";
                command = new SqlCommand(sql, connection);
                reader = command.ExecuteReader();
                reader.Read();
                imageData = (byte[])reader.GetValue(reader.GetOrdinal("Soap_image"));
                Assert.AreEqual(soaps[2].Image, Convert.ToBase64String(imageData));
                reader.Close();

                sql = "SELECT * FROM Soap_information WHERE Soap_name = '..Розовое..'";
                command = new SqlCommand(sql, connection);
                reader = command.ExecuteReader();
                reader.Read();
                imageData = (byte[])reader.GetValue(reader.GetOrdinal("Soap_image"));
                Assert.AreEqual(soaps[3].Image, Convert.ToBase64String(imageData));
                reader.Close();

                sql = "SELECT * FROM Soap_information WHERE Soap_name = '..Шампуневое..'";
                command = new SqlCommand(sql, connection);
                reader = command.ExecuteReader();
                reader.Read();
                imageData = (byte[])reader.GetValue(reader.GetOrdinal("Soap_image"));
                Assert.AreEqual(soaps[4].Image, Convert.ToBase64String(imageData));
                reader.Close();

                sql = "SELECT * FROM Soap_information WHERE Soap_name = '..Шоколадное..'";
                command = new SqlCommand(sql, connection);
                reader = command.ExecuteReader();
                reader.Read();
                imageData = (byte[])reader.GetValue(reader.GetOrdinal("Soap_image"));
                Assert.AreEqual(soaps[5].Image, Convert.ToBase64String(imageData));
                reader.Close();

                sql = "SELECT * FROM Soap_information WHERE Soap_name = '..Соляное..'";
                command = new SqlCommand(sql, connection);
                reader = command.ExecuteReader();
                reader.Read();
                imageData = (byte[])reader.GetValue(reader.GetOrdinal("Soap_image"));
                Assert.AreEqual(soaps[6].Image, Convert.ToBase64String(imageData));
                reader.Close();

                sql = "SELECT * FROM Soap_information WHERE Soap_name = '..Глиняное..'";
                command = new SqlCommand(sql, connection);
                reader = command.ExecuteReader();
                reader.Read();
                imageData = (byte[])reader.GetValue(reader.GetOrdinal("Soap_image"));
                Assert.AreEqual(soaps[7].Image, Convert.ToBase64String(imageData));
                reader.Close();

                connection.Close();
            }
        }
    }
}
